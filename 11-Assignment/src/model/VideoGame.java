package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class VideoGame {

	private StringProperty title;
	private StringProperty releaseDate;
	private StringProperty publisher;
	private IntegerProperty starRating;
	
	public VideoGame(String t, String rd, String p) {
		title = new SimpleStringProperty();
		title.set(t);
		releaseDate = new SimpleStringProperty();
		releaseDate.set(rd);
		publisher = new SimpleStringProperty();
		publisher.set(p);
		starRating = new SimpleIntegerProperty();
		starRating.set(0);
	}
	
	public void setTitle(String s) {
		title.set(s);
	}
	
	public StringProperty titleProperty() {
		return title;
	}
	
	public String getTitle() {
		return title.get();
	}
	
	public void setReleaseDate(String s) {
		releaseDate.set(s);
	}
	
	public StringProperty releaseDateProperty() {
		return releaseDate;
	}
	
	public String getReleaseDate() {
		return releaseDate.get();
	}
	
	public void setPublisher(String p) {
		title.set(p);
	}
	
	public StringProperty publisherProperty() {
		return publisher;
	}
	
	public String getPublisher() {
		return publisher.get();
	}
	
	public void setRating(int i) {
		starRating.set(i);
	}
	
	public IntegerProperty ratingProperty() {
		return starRating;
	}
	
	public int getRating() {
		return starRating.get();
	}
}
